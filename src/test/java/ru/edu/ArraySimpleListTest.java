package ru.edu;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ArraySimpleListTest<T> {

    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    private ArraySimpleList<String> list;

    @Before
    public void setUp() throws Exception {
        int capasity = 2;
        list = new ArraySimpleList<>(capasity);
    }

    @Test
    public void testList() {
        assertEquals(0, list.size());
    }

    @Test
    public void add() {
        list.add(FIRST);
        assertEquals(1, list.size());

        list.add(SECOND);
        assertEquals(2, list.size());

    }

    @Test
    public void set() {
        list.add(FIRST);
        list.add(SECOND);
        list.set(1,"newElement");
        System.out.println("Элемент №2 = " + list.get(1));
        assertEquals("newElement", list.get(1));
    }

    @Test
    public void get() {
        list.add(FIRST);
        list.add(SECOND);
        System.out.println("Элемент №1 = " + list.get(0));
        assertEquals("First", list.get(0));
        System.out.println("Элемент №2 = " + list.get(1));
        assertEquals("Second", list.get(1));
    }

    @Test
    public void remove() {
        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i));
            System.out.print(list.get(i) + " ");
        }
        System.out.println("");
        list.remove(3);
        for (int i = 0; i < 10; i++) {
        System.out.print(list.get(i) + " ");
        }
    }

    @Test
    public void indexOf() {
        list.add(FIRST);
        System.out.print(list.indexOf(FIRST) + ", ");
        assertEquals(0, list.indexOf(FIRST));
        list.add(SECOND);
        System.out.print(list.indexOf(SECOND));
        assertEquals(1, list.indexOf(SECOND));
    }

    @Test
    public void size() {
        list.add(FIRST);
        list.add(SECOND);
        System.out.print(list.size());
        assertEquals(2, list.size());
    }
}