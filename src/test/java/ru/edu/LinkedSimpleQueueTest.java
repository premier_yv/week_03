package ru.edu;

import org.junit.Before;
import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.*;

public class LinkedSimpleQueueTest {

    private SimpleQueue<String> list;


    @Before
    public void setUp() throws Exception {
        Integer capacity=5;
        list = new LinkedSimpleQueue <>();
    }

    @Test
    public void offer() {
        assertEquals(true, list.offer("First"));
        assertEquals(true, list.offer("Second"));
    }

    @Test
    public void poll() {
        assertEquals(true, list.offer("First"));
        assertEquals(true, list.offer("Second"));
        assertEquals("First",list.poll());
    }

    @Test
    public void peek() {
        assertEquals(true, list.offer("First"));
        assertEquals(true, list.offer("Second"));
        assertEquals(true, list.offer("Third"));
        assertEquals("First", list.peek());
        list.poll();
        assertEquals("Second", list.peek());
    }

    @Test
    public void size() {
        assertEquals(0,list.size());
        assertEquals(true, list.offer("First"));
        assertEquals(true, list.offer("Second"));
        assertEquals(2,list.size());
        assertEquals(true, list.offer("Third"));
        assertEquals(3,list.size());
    }

    @Test
    public void capacity() {
        assertEquals(-1,list.capacity());
    }
}