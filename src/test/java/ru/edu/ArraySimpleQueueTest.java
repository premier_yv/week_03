package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleQueueTest {

    private ArraySimpleQueue<String> list;

    @Before
    public void setUp() throws Exception {
        int capasity = 5;
        list = new ArraySimpleQueue<>(capasity);
    }

    @Test
    public void offer() {
        boolean success;
        success = list.offer("elementAdd1");
        assertEquals(true,success);
       list.offer("elementAdd2");
        success = list.offer("elementAdd3");
        assertEquals(true,success);
        list.offer("elementAdd4");
        list.offer("elementAdd5");
        success = list.offer("elementAdd6");
        assertEquals(false,success);

    }

    @Test
    public void poll() {
       assertEquals("Очередь пуста",list.poll());
        list.offer("elementAdd1");
        list.offer("elementAdd2");
        list.offer("elementAdd3");
        for (int i = 0; i < 5; i++) {
            System.out.print(list.poll() + ", ");
        }
 //      assertEquals("elementAdd1",list.poll());
    }

    @Test
    public void peek() {
        assertEquals(null,list.peek());
        list.offer("elementAdd1");
        list.offer("elementAdd2");
        assertEquals("elementAdd1",list.peek());
    }

    @Test
    public void size() {
        assertEquals(0,list.size());
        list.offer("elementAdd1");
        list.offer("elementAdd2");
        assertEquals(2,list.size());
        list.offer("elementAdd3");
        assertEquals(3,list.size());
    }

    @Test
    public void capacity() {
        assertEquals(5,list.capacity());
        list.offer("elementAdd1");
        list.offer("elementAdd2");
        assertEquals(5,list.capacity());
    }
}