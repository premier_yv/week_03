package ru.edu;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LinkedSimpleListTest {

    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    private SimpleList<String> list;

    @Before
    public void setUp() throws Exception {
        list = new LinkedSimpleList<>();
    }

    @Test
    public void testList(){
        assertEquals(0,list.size());

        list.add(FIRST);
        assertEquals(1,list.size());

        assertEquals(FIRST, list.get(0));

        list.add(SECOND);
        assertEquals(2,list.size());

        assertEquals(SECOND, list.get(1));

    }
    @Test
    public void testBig(){
        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i));
        }

            list.get(3);
            list.get(7);

        list.set(2,"new2");
        System.out.println(list.get(2));
        assertEquals("new2",list.get(2));
        list.set(9,"new9");
        System.out.println(list.get(9));
        assertEquals("new9",list.get(9));
        list.set(12,"new2");


        list.remove(5);

    }
}