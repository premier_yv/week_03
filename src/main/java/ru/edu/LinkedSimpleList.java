package ru.edu;

public class LinkedSimpleList<T> implements SimpleList<T> {

    /**
     * Определение переменной head, ссыдающейся на начало списка.
     */
    private Node<T> head;

    /**
     * Определение переменной tail, ссыдающейся на конец списка.
     */
    private Node<T> tail;

    /**
     * Определение переменной size, хранящей количество элементов списка.
     */
    private int size;

    /**
     * Создание вложенного класса Node.
     *
     * @param <T>
     */
    private static class Node<T> {
        /**
         * Определение переменной prev, ссыдающейся на предыдущий элемент.
         */
        private Node prev;

        /**
         * Определение переменной value, хранащей значение элемента.
         */
        private T value;

        /**
         * Определение переменной next, ссыдающейся на следующий элемент.
         */
        private Node next;

        Node(final T valueArg) {
            this.value = valueArg;
        }
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        Node<T> node = new Node<>(value);

        if (head == null) {
            head = node;
            tail = node;
        } else {
            node.prev = tail;
            tail.next = node;
            tail = node;
        }
        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        if (index > size) {
            System.out.println("Позиция элемента должна быть не более " + size);
            return;
        }
        Node<T> node = new Node<>(value);

        if (index < size / 2) {
            node = head;

            for (int i = 0; i < index; i++) {
                node = node.next;
            }
        } else {
            node = tail;

            for (int i = 0; i < size - 1 - index; i++) {
                node = node.prev;
            }
        }
        node.value = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        Node<T> node = null;
        if (index < size / 2) {
            node = head;

            for (int i = 0; i < index; i++) {
                node = node.next;
            }
        } else {
            node = tail;

            for (int i = 0; i < size - 1 - index; i++) {
                node = node.prev;
            }
        }
        return node.value;
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if (size == 1) {
            head = null;
            tail = null;
            size = 0;
            return;
        }
        if (index == 0) {
            Node next = head.next;
            if (next != null) {
                next.prev = null;
            }
            head = next;
            size--;
            return;
        }

        if (index == size - 1) {
            Node prev = tail.prev;
            prev.next = null;
            tail = prev;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            if (node.value.equals(value)) {
                return i;
            }
            node = node.next;
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
