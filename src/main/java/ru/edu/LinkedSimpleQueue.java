package ru.edu;

public class LinkedSimpleQueue<Object> implements SimpleQueue<Object> {

    /**
     * Объявление переменной head.
     */
    private Node<Object> head;

    /**
     * Объявление переменной tail.
     */
    private Node<Object> tail;

    /**
     * Объявление переменной size.
     */
    private int size;

    /**
     * Объявление вложенного класса Node.
     *
     * @param <Object>
     */
    private static class Node<Object> {
        /**
         * Объявление переменной prev, ссылающейся на предыдущий элемент.
         */
        private Node prev;

        /**
         * Объявление переменной value, хранящей значение элемента.
         */
        private Object value;

        Node(final Object valueArg) {
            this.value = valueArg;
        }
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final Object value) {
        Node<Object> node = new Node<>(value);
        if (head == null & head == tail) {
            head = node;
            tail = node;
        } else {
            node.prev = tail;
            tail = node;
        }
        size++;
        node.value = value;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public Object poll() {
    Node tmp = null;
    String msg = "Очередь пуста";
        Node<Object> node = new Node<>(tail.value);
    if (head == tail || head == null) {
        return (Object) msg;
    }

    // First <- Second <- Third

        tmp = head;
        head = tail;
        node = tail;
        while (node.prev != tmp) {
            head = node.prev;
            node = head;
        }
        head.prev = null;
        size--;
        return (Object) tmp.value;

    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public Object peek() {
        return (Object) head.value;
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        Node<Object> node = new Node<>(null);
        int sizeArg = 0;
        if (head != tail & head != null) {
            node = tail;
            sizeArg++;
            while (node != head) {
                node = node.prev;
                sizeArg++;
            }
        } else {
            return 0;
        }
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return -1;
    }
}
