package ru.edu;

public class ArraySimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Объявление массива arr.
     */
    private final T[] arr;

    /**
     * Объвление переменной head.
     */
    private int head = 0;

    /**
     * Объявление переменной tail.
     */
    private int tail = 0;

    /**
     * Определение конструктора ArraySimpleQueue.
     *
     * @param capacity количество элементов
     */
    public ArraySimpleQueue(final int capacity) {
        arr = (T[]) new Object[capacity];
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        try {
            if ((head == tail) & (arr[head] == null)) {
                arr[tail] = value;
                return true;
            } else {
                tail++;
                arr[tail] = value;
                return true;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
        }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        T tmp;
        head = 0;
        if (arr[head] != null) {
            tmp = arr[head];
            while (head < tail) {
                arr[head] = arr[head + 1];
                head++;
            }
            arr[tail] = null;
            return tmp;
        } else {
            return (T) "Очередь пуста";
        }
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        return arr[head];
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        int i = 0;
        if (arr[0] != null) {
            while (arr[i] != null) {
                i++;
            }
            return i;
        } else {
            return 0;
        }
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
       return arr.length;
    }
}
